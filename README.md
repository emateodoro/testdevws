# Test Web Service

This is a test application created in order to demo a few java technologies and libraries.

The following libraries / frameworks were included:

- SpringBoot (Web + JPA)
- Lombok

## Requirements

In order to build and run the application locally, you will need the following:


- Java 8+ (JDK)
- Maven
- MySQL or MariaDB installed and running

If you would prefer to run a pre-built container, you can pull from the public docker hub repo:

```
registry.hub.docker.com/etsousa/testdevws:latest
```


## Environment Variables and Configuration

The following variables can be set in order to customize the app

| Name          | Default       |
| ------------- |:-------------:|
| APP_PORT      | 8080          |
| MYSQL_HOST    | localhost     |
| MYSQL_PORT    | 3306          |
| MYSQL_DBNAME  | testdevws     |
| MYSQL_USERNAME| root          |
| MYSQL_PASSWORD| root          |




## Endpoints

Considering the default configurations, you have the following endpoints available:


### Rest Endpoint

| URL           | Description   | Method |
| ------------- |:-------------:| :--:|
| /services/user/{username}        |  Returns a XML object containing all items for a specific user | GET|



