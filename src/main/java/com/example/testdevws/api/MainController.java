package com.example.testdevws.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.testdevws.model.UserDto;
import com.example.testdevws.service.UserService;

@RestController
@RequestMapping("/services")
public class MainController {
	
	@Autowired
	private UserService userService;

	@GetMapping(value = "/user/{username}", produces = {MediaType.APPLICATION_XML_VALUE})
	public ResponseEntity<UserDto> getUserItems(@PathVariable("username") String username) {
		UserDto userDto = userService.getUserByUsername(username);
		return new ResponseEntity<>(userDto, HttpStatus.OK);
	}
}