package com.example.testdevws.service;

import com.example.testdevws.model.UserDto;

public interface UserService {
	public UserDto getUserByUsername(String username);
}
