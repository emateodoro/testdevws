package com.example.testdevws.service;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.testdevws.model.ItemDto;
import com.example.testdevws.model.PropertyDto;
import com.example.testdevws.model.UserDto;
import com.example.testdevws.persistence.entity.Item;
import com.example.testdevws.persistence.entity.Property;
import com.example.testdevws.persistence.entity.User;
import com.example.testdevws.persistence.repository.UserRepository;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private ModelMapper modelMapper;
	
	@Override
	public UserDto getUserByUsername(String username) {
		UserDto userDto = null;
		if(username != null && !username.isEmpty()) {
			User user = userRepository.findByUsername(username);
			if(user != null) {
				userDto = convertUserToDto(user);
			}
		}
		
		return userDto;
	}

	private UserDto convertUserToDto(User user) {
		UserDto userDto = modelMapper.map(user, UserDto.class);
		
		List<ItemDto> itemsDto = null;
		if (user.getItems() != null) {
			itemsDto = user.getItems().stream()
							.map(this::convertItemToDto)
							.collect(Collectors.toList());
		}

		userDto.setItems(itemsDto);
		
		return userDto;
	}
	
	private ItemDto convertItemToDto(Item item) {
		ItemDto itemDto = modelMapper.map(item, ItemDto.class);
		
		List<PropertyDto> propertiesDto = null;
		if (item.getProperties() != null) {
			propertiesDto = item.getProperties().stream()
							.map(this::convertPropertyToDto)
							.collect(Collectors.toList());
		}
		
		itemDto.setProperties(propertiesDto);
		
		return itemDto;
	}

	private PropertyDto convertPropertyToDto(Property property) {
		return modelMapper.map(property, PropertyDto.class);
	}
}
