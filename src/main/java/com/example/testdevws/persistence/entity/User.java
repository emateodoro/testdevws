package com.example.testdevws.persistence.entity;

import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "user")
public class User {

	@Id
	@Column(name="idUser")
	private Long id;
	
	@Column(name="username")
	private String username;
	
	@OneToMany(mappedBy = "user", fetch = FetchType.EAGER)
    private List<Item> items;
}
