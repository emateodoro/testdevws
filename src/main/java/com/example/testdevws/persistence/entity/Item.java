package com.example.testdevws.persistence.entity;

import java.sql.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "item")
public class Item {

	@Id
	@Column(name="idItem")
	private Long id;

	@Column(name="name")
	private String name;
    
	@Column(name="game")
	private String game;
    
	@Column(name="expirationDate")
	private Date expirationDate;

	@Column(name="quantity")
	private Integer quantity;
    
	@OneToMany(mappedBy = "item", fetch = FetchType.EAGER)
	private List<Property> properties;
    
	@ManyToOne(fetch = FetchType.LAZY, optional = true)
	@JoinColumn(name = "idUser", nullable = true)
	private User user;
}
