package com.example.testdevws.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.example.testdevws.persistence.entity.User;

public interface UserRepository extends JpaRepository<User, Long> {

	User findByUsername(String username);
}
