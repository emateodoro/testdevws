package com.example.testdevws.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;
import org.modelmapper.ModelMapper;

import com.example.testdevws.model.UserDto;
import com.example.testdevws.persistence.entity.Item;
import com.example.testdevws.persistence.entity.Property;
import com.example.testdevws.persistence.entity.User;
import com.example.testdevws.persistence.repository.UserRepository;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceTest {
	
	
	@Mock
	UserRepository userRepository;
	
	@Spy
	ModelMapper mapper = new ModelMapper();
	
	@InjectMocks
	UserService userService = new UserServiceImpl();
	
	
	@Before
	public void init() {
		List<User> mockedUsers = new ArrayList<>();
		List<Property> props = Arrays.asList(new Property(1L, "p1", "v1", null));
		List<Item> items = Arrays.asList(new Item(1L, "name", "game1",null, 3, props, null));
		User user1 = new User(1L, "user1", items);
		mockedUsers.add(user1);
		
		Mockito.doReturn(user1).when(userRepository).findByUsername("user1");
	}
	
	@Test
	public void testSingleUserMapping() {
		
		UserDto user = userService.getUserByUsername("user1");
		
		Assert.assertNotNull(user);
		Assert.assertEquals(1L, (long) user.getId());
		Assert.assertEquals("user1", user.getUsername());
		Assert.assertNotNull(user.getItems());
		Assert.assertEquals(1, user.getItems().size());
		Assert.assertEquals("name", user.getItems().get(0).getName());
		Assert.assertEquals("game1", user.getItems().get(0).getGame());
		Assert.assertEquals(3, (int)user.getItems().get(0).getQuantity());
		Assert.assertNotNull(user.getItems().get(0).getProperties());
		Assert.assertEquals(1, user.getItems().get(0).getProperties().size());
		Assert.assertEquals("p1", user.getItems().get(0).getProperties().get(0).getName());
		Assert.assertEquals("v1", user.getItems().get(0).getProperties().get(0).getValue());
	}
	
	@Test
	public void testSingleUserNotFound() {
		
		UserDto user = userService.getUserByUsername("user3");
		
		Assert.assertNull(user);
	}
	
	@Test
	public void testSingleUserNullUsername() {
		
		UserDto user = userService.getUserByUsername(null);
		
		Assert.assertNull(user);
	}
	
	@Test
	public void testSingleUserEmptyUsername() {
		
		UserDto user = userService.getUserByUsername("");
		
		Assert.assertNull(user);
	}
}
